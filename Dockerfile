FROM java:8
MAINTAINER river-and-boat
VOLUME /tmp
ADD build/libs/UserService-0.0.1-SNAPSHOT.jar UserService-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/UserService-0.0.1-SNAPSHOT.jar"]
