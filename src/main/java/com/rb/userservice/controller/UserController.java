package com.rb.userservice.controller;

import com.rb.userservice.modekl.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {
    private final Map<String, User> source = new HashMap<>();

    public UserController() {

        source.put("JYZ", new User("Jiang Yuzhou", 26, "male"));
        source.put("DKY", new User("Diao Keyu", 25, "male"));
        source.put("ZJJ", new User("Zhang Jinjin", 25, "female"));
        source.put("ZYM", new User("Zhang Yunmeng", 25, "female"));
    }

    @GetMapping("/users/{key}")
    public ResponseEntity<User> getUser(@PathVariable String key) {
        return new ResponseEntity<>(source.get(key), HttpStatus.OK);
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> getUsers() {
        return new ResponseEntity<>(new ArrayList<>(source.values()), HttpStatus.OK);
    }
}
