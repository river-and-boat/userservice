package com.rb.userservice.modekl;

import java.util.ArrayList;
import java.util.List;

public class User {
    public String name;
    public int age;
    public String sex;
    List<Book> books = new ArrayList<>();

    public User(String name, int age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        books.add(new Book("HaliBote", 26.6D));
    }
}
