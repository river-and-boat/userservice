package com.rb.userservice.controller;

import com.rb.userservice.modekl.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserControllerTest {
    private UserController userController;

    @BeforeEach
    public void init() {
        userController = new UserController();
    }

    @Test
    public void should_get_JYZ_when_key_is_JYZ() {
        User user = userController.getUser("JYZ").getBody();
        assertEquals("Jiang Yuzhou", Objects.requireNonNull(user).name);
    }
}